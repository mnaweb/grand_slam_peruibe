package br.com.mna.jfx.dao;

import br.com.mna.jfx.model.Jogador;
import java.util.List;

/**
 * Contrato de persistência para a entidade <code>Mercadoria</code>. 
 * 
 * <p>Define as operações basicas de cadastro (CRUD), seguindo o design pattern <code>Data Access Object</code>.</p>
 * 
 * @author YaW Tecnologia
 */
public interface JogadorDAO {

    /**
     * Faz a inserção ou atualização da jogador na base de dados.
     *
     * @param jogador
     * @return referência atualizada do objeto.
     * @throws <code>RuntimeException</code> se algum problema ocorrer.
     */
    Jogador save(Jogador jogador);

    /**
     * Exclui o registro da jogador na base de dados
     *
     * @param jogador
     * @throws <code>RuntimeException</code> se algum problema ocorrer.
     */
    void remove(Jogador jogador);

    /**
     * @return Lista com todas as jogadors cadastradas no banco de dados.
     * @throws <code>RuntimeException</code> se algum problema ocorrer.
     */
    List<Jogador> getAll();

    /**
     * @param nome Filtro da pesquisa de jogadors.
     * @return Lista de jogadors com filtro em nome.
     * @throws <code>RuntimeException</code> se algum problema ocorrer.
     */
    List<Jogador> getJogadoresByNome(String nome);

    /**
     * @param id filtro da pesquisa.
     * @return Mercadoria com filtro no id, caso nao exista
     * retorna <code>null</code>.
     * @throws <code>RuntimeException</code> se algum problema ocorrer.
     */
    Jogador findById(Integer id);
}
