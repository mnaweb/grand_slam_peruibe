package br.com.mna.jfx.ui;

import br.com.mna.jfx.model.Jogador;
import java.text.ParseException;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * <code>TableView</code> adaptada para apresentar objetos <code>Jogador</code>.
 *
 * @author YaW Tecnologia
 */
public class JogadorTableView extends TableView<JogadorTableView.JogadorItem> {

    private ObservableList<JogadorItem> jogadores;

    public JogadorTableView() {
        TableColumn<JogadorItem, String> idCol = new TableColumn<>("Id");
        idCol.setMinWidth(80);
        idCol.setCellValueFactory(new PropertyValueFactory<JogadorItem, String>("id"));

        TableColumn<JogadorItem, String> nomeCol = new TableColumn<>("Nome");
        nomeCol.setMinWidth(190);
        nomeCol.setCellValueFactory(new PropertyValueFactory<JogadorItem, String>("nome"));

        TableColumn<JogadorItem, String> descricaoCol = new TableColumn<>("Descrição");
        descricaoCol.setMinWidth(200);
        descricaoCol.setCellValueFactory(new PropertyValueFactory<JogadorItem, String>("descricao"));

        jogadores = FXCollections.observableArrayList();
        setItems(jogadores);
        
        getColumns().addAll(idCol, nomeCol, descricaoCol);
    }

    public void reload(final List<Jogador> jogadores) {
        this.jogadores.clear();
        Platform.runLater(new Runnable(){
            @Override
            public void run() {
                for (Jogador m: jogadores) {
                    JogadorItem item = new JogadorItem(m);
                    JogadorTableView.this.jogadores.add(item);
                }
            }
            
        });
    }

    public Jogador getSelectedItem() {
        JogadorItem item = getSelectionModel().getSelectedItem();
        if (item != null) {
            return item.toJogador();
        }
        return null;
    }

    /**
     * Item da tabela, faz o binding da <code>Jogador</code> com <code>TableView</code>.
     */
    public static class JogadorItem {

        private final SimpleStringProperty id;
        private final SimpleStringProperty nome;
        private final SimpleStringProperty descricao;

        private JogadorItem(Jogador m) {
            this.id = new SimpleStringProperty(m.getId() + "");
            this.nome = new SimpleStringProperty(m.getNome());
            this.descricao = new SimpleStringProperty(m.getDescricao());
        }

        public String getId() {
            return id.get();
        }
        
        public String getNome() {
            return nome.get();
        }

        public String getDescricao() {
            return descricao.get();
        }

        public Jogador toJogador(){
            return new Jogador(Integer.valueOf(this.id.get()),
                                this.nome.get(),
                                this.descricao.get());
        }
    }
}
