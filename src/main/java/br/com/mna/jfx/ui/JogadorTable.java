package br.com.mna.jfx.ui;

import br.com.mna.jfx.model.Jogador;
import java.util.List;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

/**
 * Reune os componentes para formar uma tabela de <code>Mercadoria</code>.
 * 
 * @see br.com.yaw.jfx.ui.MercadoriaTableView
 * 
 * @author YaW Tecnologia
 */
public class JogadorTable extends VBox {
    
    private JogadorTableView table;
    
    public JogadorTable(){
        table = new JogadorTableView();
        this.getChildren().addAll(table);
        this.setPadding(new Insets(30, 0, 0, 10));//css
    }
    
    public void reload(List<Jogador> mercadorias) {
        table.reload(mercadorias);
    }

    public void setMouseEvent(EventHandler<MouseEvent> event) {
        table.setOnMouseClicked(event);
    }
    
    public Jogador getMercadoriaSelected() {
        return table.getSelectedItem();
    }
}
