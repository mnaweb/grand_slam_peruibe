package br.com.mna.jfx.ui;

import javafx.animation.FadeTransition;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Tela principal da aplicação. Apresenta uma lista com as mercadorias cadastradas. 
 * 
 * <p>A partir dessa tela é possível criar/editar ou pesquisar mercadoria.</p>
 * 
 * @author YaW Tecnologia
 */
public class PrincipalView {

    private Scene mainScene;
    private MenuItem menuSobre;
    private MenuItem menuJogador;
    
    public PrincipalView(Stage stage) {
        Group panel = new Group();
        mainScene = new Scene(panel);
        mainScene.getStylesheets().add("style.css");
        
        MenuBar menuBar = getMenuBar();
        menuBar.prefWidthProperty().bind(stage.widthProperty());
        
        panel.getChildren().addAll(menuBar);
        
        stage.setTitle("Grand slam peruibe");
        stage.setWidth(700);
        stage.setHeight(510);
        stage.setScene(mainScene);
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.show();
    }
        
    public void addTransition() {
        FadeTransition ft = new FadeTransition(Duration.millis(2000));
        ft.setFromValue(0.2);
        ft.setToValue(1);
        ft.setAutoReverse(true);
        ft.play();
    }
    
    private MenuBar getMenuBar() {
        MenuBar menuBar = new MenuBar();
        Menu menuAjuda = new Menu("Ajuda");
        menuSobre = new MenuItem("Sobre");
        menuSobre.setId("exibirSobre");
        menuSobre.setAccelerator(KeyCombination.keyCombination("F1"));

        Menu menuCadastro = new Menu("Cadastro");
        menuJogador = new MenuItem("Jogador");
        menuJogador.setId("listaJogador");

        menuAjuda.getItems().addAll(menuSobre);
        menuCadastro.getItems().addAll(menuJogador);

        menuBar.getMenus().addAll(menuCadastro);
        menuBar.getMenus().addAll(menuAjuda);

        return menuBar;
    }

    public MenuItem getMenuSobre() {
        return menuSobre;
    }

    public MenuItem getMenuJogador() {
        return menuJogador;
    }
    
}
