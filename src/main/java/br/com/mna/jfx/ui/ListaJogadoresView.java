package br.com.mna.jfx.ui;

import br.com.mna.jfx.model.Jogador;
import java.util.List;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Tela principal da aplicação. Apresenta uma lista com as mercadorias cadastradas. 
 * 
 * <p>A partir dessa tela é possível criar/editar ou pesquisar mercadoria.</p>
 * 
 * @author YaW Tecnologia
 */
public class ListaJogadoresView extends Stage {

    private Scene mainScene;
    private JogadorTable tabela;
    private Button bNewMercadoria;
    private Button bRefreshLista;
    private Button bFindMercadoria;
    
    public ListaJogadoresView() {
        inicializaComponentes();
        Group panel = new Group();
        mainScene = new Scene(panel);
        mainScene.getStylesheets().add("style.css");
        
        HBox boxButtons = getButtonsBox();
        
        panel.getChildren().addAll(boxButtons, tabela);
        
        setTitle("Lista de jogadores");
        setWidth(700);
        setHeight(510);
        setScene(mainScene);
        setResizable(false);
        centerOnScreen();
    }
    
    private void inicializaComponentes() {
        tabela = new JogadorTable();
        
        bNewMercadoria = new Button("Nova");
        bNewMercadoria.getStyleClass().add("buttonGreen");
        bNewMercadoria.setId("incluirMercadoria");
        bFindMercadoria = new Button("Buscar");
        bFindMercadoria.getStyleClass().add("buttonLarge");
        bFindMercadoria.setId("buscarMercadorias");
        bRefreshLista = new Button("Atualizar");
        bRefreshLista.getStyleClass().add("buttonWhite");
        bRefreshLista.setId("atualizarMercadorias");
    }
    
    public void addTransition() {
        disableButtonBar(true);
        FadeTransition ft = new FadeTransition(Duration.millis(2000), tabela);
        ft.setFromValue(0.2);
        ft.setToValue(1);
        ft.setAutoReverse(true);
        ft.play();
        
        ft.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                disableButtonBar(false);
            }
        });
    }
    
    private HBox getButtonsBox() {
        HBox box = new HBox();
        box.getChildren().addAll(bNewMercadoria, bFindMercadoria, bRefreshLista);
        box.getStyleClass().add("buttonBarMain");
        return box;
    }
    
    public Button getNewButton() {
        return bNewMercadoria;
    }

    public Button getRefreshButton() {
        return bRefreshLista;
    }

    public Button getFindButton() {
        return bFindMercadoria;
    }

    public JogadorTable getTabela() {
        return tabela;
    }
    
    public void refreshTable(List<Jogador> mercadorias) {
        tabela.reload(mercadorias);        
    }

    private void disableButtonBar(boolean disable) {
        bNewMercadoria.setDisable(disable);
        bFindMercadoria.setDisable(disable);
        bRefreshLista.setDisable(disable);
    }

}
