package br.com.mna.jfx.ui;

import br.com.mna.jfx.model.Jogador;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Tela para incluir/editar o registro da <code>Jogador</code>.
 * 
 * <p>
 *  Essa tela trabalha em modo inclusão ou edicão de <code>Jogador</code>.
 *  Em edicão é possível acionar a funcionalidade para remover a <code>Jogador</code>.
 * </p>
 * 
 * @author YaW Tecnologia
 */
public class IncluirJogadorView extends Stage {

    private TextField tfId;
    private TextField tfNome;
    private TextField tfQuantidade;
    private TextField tfDescricao;
    private TextField tfPreco;
    private TextField tfVersion;
    private Button bSalvar;
    private Button bCancelar;
    private Button bExcluir;

    public IncluirJogadorView() {
        setTitle("Incluir Jogador");
        setWidth(300);
        setHeight(230);
        setResizable(false);
        initModality(Modality.APPLICATION_MODAL);
        
        inicializaComponentes();
    }

    private void inicializaComponentes() {
        VBox box = new VBox();
        box.getChildren().addAll(buildInputs(), buildBotoes());
        
        Scene scene = new Scene(new Group(box));
        scene.getStylesheets().add("style.css");
        this.setScene(scene);
        this.resetForm();
    }

    private HBox buildBotoes() {
        bSalvar = new Button("Salvar");
        bSalvar.setId("salvarJogador");
        bSalvar.setDefaultButton(true);

        bCancelar = new Button("Cancelar");
        bCancelar.setId("cancelarSalvarJogador");
        bCancelar.setCancelButton(true);
        
        bExcluir = new Button("Excluir");
        bExcluir.setId("excluirJogador");
        bExcluir.getStyleClass().add("buttonDanger");

        HBox box = new HBox();
        box.getChildren().addAll(bSalvar, bCancelar, bExcluir);
        box.getStyleClass().add("buttonBar");
        
        return box;
    }

    private GridPane buildInputs() {
        tfId = new TextField();
        tfId.setPromptText("");
        tfId.setId("0");
        tfId.setEditable(false);
        tfId.setMinWidth(90);
        tfId.setMaxWidth(90);

        tfNome = new TextField();
        tfNome.setPromptText("*Campo obrigatório");
        tfNome.setMinWidth(180);
        tfNome.setMaxWidth(180);

        tfDescricao = new TextField();
        tfDescricao.setPromptText("");
        tfDescricao.setMinWidth(180);
        tfDescricao.setMaxWidth(180);

        tfQuantidade = new TextField();
        tfQuantidade.setPromptText("*Campo obrigatório");
        tfQuantidade.setMinWidth(90);
        tfQuantidade.setMaxWidth(90);

        tfPreco = new TextField();
        tfPreco.setPromptText("*Campo obrigatório");
        tfPreco.setMinWidth(90);
        tfPreco.setMaxWidth(90);

        tfVersion = new TextField();
        tfVersion.setVisible(false);
        
        GridFormBuilder grid = new GridFormBuilder();
        grid.addRow(new Label("Id: "), tfId)
                .addRow(new Label("Nome: "), tfNome)
                .addRow(new Label("Descrição: "), tfDescricao);
        
        return grid.build();
    }

    public final void resetForm() {
        tfId.setText("");
        tfNome.setText("");
        tfDescricao.setText("");
        tfPreco.setText("");
        tfQuantidade.setText("");
        tfVersion.setText("");
        bExcluir.setVisible(false);
    }

    private void populaTextFields(Jogador m) {
        tfId.setText(m.getId().toString());
        tfNome.setText(m.getNome());
        tfDescricao.setText(m.getDescricao() == null ? "" : m.getDescricao());
    }


    private Jogador loadJogadorFromPanel() {
        String nome = null;
        if (!tfNome.getText().trim().isEmpty()) {
            nome = tfNome.getText().trim();
        }
        
        String descricao = null;
        if (!tfDescricao.getText().trim().isEmpty()) {
            descricao = tfDescricao.getText().trim();
        }

        Integer id = null;
        try {
            id = Integer.parseInt(tfId.getText());
        } catch (Exception nex) {}

        return new Jogador(id, nome, descricao);
    }

    public void setJogador(Jogador m) {
        resetForm();
        if (m != null) {
            populaTextFields(m);
            bExcluir.setVisible(true);
        }
    }
    
    public Integer getJogadorId() {
        try {
            return Integer.parseInt(tfId.getText());
        } catch (Exception nex) {
            return null;
        }
    }

    public Jogador getJogador() {
        return loadJogadorFromPanel();
    }

    public Button getSalvarButton() {
        return bSalvar;
    }

    public Button getCancelarButton() {
        return bCancelar;
    }
    
    public Button getExcluirButton() {
        return bExcluir;
    }
}
