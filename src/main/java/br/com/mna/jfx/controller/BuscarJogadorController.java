package br.com.mna.jfx.controller;

import br.com.mna.jfx.action.AbstractAction;
import br.com.mna.jfx.action.BooleanExpression;
import br.com.mna.jfx.action.ConditionalAction;
import br.com.mna.jfx.dao.JogadorDAO;
import br.com.mna.jfx.dao.JogadorDAOJPA;
import br.com.mna.jfx.event.BuscarJogadorEvent;
import br.com.mna.jfx.model.Jogador;
import br.com.mna.jfx.ui.BuscarJogadorView;
import java.util.List;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

/**
 * Define a <code>Controller</code> responsável por gerir a tela de Busca de <code>Mercadoria</code> pelo campo <code>nome</code>.
 * 
 * @see br.com.yaw.jfx.controller.PersistenceController
 * 
 * @author YaW Tecnologia
 */
public class BuscarJogadorController extends PersistenceController {
    
    private BuscarJogadorView view;
    
    public BuscarJogadorController(ListaJogadorController parent) {
        super(parent);
        this.view = new BuscarJogadorView();
        
        this.view.addEventHandler(WindowEvent.WINDOW_HIDDEN, new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent window) {
                BuscarJogadorController.this.cleanUp();
            }
        });
        
        registerAction(this.view.getCancelarButton(), new AbstractAction() {
            @Override
            protected void action() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        view.hide();
                    }
                });
            }
        });
        
        registerAction(view.getBuscarButton(), ConditionalAction.build()
                .addConditional(new BooleanExpression() {
                    @Override
                    public boolean conditional() {
                        return view.getText().length() > 0;
                    }
                })
                .addAction(new AbstractAction() {
                    private List<Jogador> list;

                    @Override
                    protected void action() {
                        JogadorDAO dao = new JogadorDAOJPA(getPersistenceContext());
                        list = dao.getJogadoresByNome(view.getText());
                    }

                    @Override
                    public void posAction() {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                view.hide();
                            }
                        });
                        fireEvent(new BuscarJogadorEvent(list));
                        list = null;
                    }
                }));
    }
    
    public void show() {
        loadPersistenceContext();
        view.show();
    }

    @Override
    protected void cleanUp() {
        view.resetForm();
	super.cleanUp();
    }
}
