package br.com.mna.jfx.controller;

import br.com.mna.jfx.action.AbstractAction;
import br.com.mna.jfx.action.BooleanExpression;
import br.com.mna.jfx.action.ConditionalAction;
import br.com.mna.jfx.action.TransactionalAction;
import br.com.mna.jfx.dao.JogadorDAO;
import br.com.mna.jfx.dao.JogadorDAOJPA;
import br.com.mna.jfx.event.DeletarJogadorEvent;
import br.com.mna.jfx.event.IncluirJogadorEvent;
import br.com.mna.jfx.model.Jogador;
import br.com.mna.jfx.ui.Dialog;
import br.com.mna.jfx.ui.IncluirJogadorView;
import br.com.mna.jfx.validation.JogadorValidator;
import br.com.mna.jfx.validation.Validator;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

/**
 * Define a <code>Controller</code> responsável por gerir a tela de inclusão/edição de <code>Jogador</code>.
 * 
 * @see br.com.yaw.jfx.controller.PersistenceController
 * 
 * @author YaW Tecnologia
 */
public class IncluirJogadorController extends PersistenceController {
    
    private IncluirJogadorView view;
    private Validator<Jogador> validador = new JogadorValidator();
    
    public IncluirJogadorController(AbstractController parent) {
        super(parent);
        
        this.view = new IncluirJogadorView();
        this.view.addEventHandler(WindowEvent.WINDOW_HIDDEN, new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent window) {
                IncluirJogadorController.this.cleanUp();
            }
        });
        
        registerAction(this.view.getCancelarButton(), new AbstractAction() {
            @Override
            protected void action() {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        view.hide();
                    }
                });
            }
        });
        
        registerAction(this.view.getSalvarButton(), 
                ConditionalAction.build()
                    .addConditional(new BooleanExpression() {
                        @Override
                        public boolean conditional() {
                            Jogador m = view.getJogador();
                            String msg = validador.validate(m);
                            if (!"".equals(msg == null ? "" : msg)) {
                                Dialog.showInfo("Validacão", msg, view);
                                return false;
                            }
                                                
                            return true;
                        }
                    })
                    .addAction(
                        TransactionalAction.build()
                            .persistenceCtxOwner(IncluirJogadorController.this)
                            .addAction(new AbstractAction() {
                                private Jogador m;

                                @Override
                                protected void action() {
                                    m = view.getJogador();
                                    JogadorDAO dao = new JogadorDAOJPA(getPersistenceContext());
                                    m = dao.save(m);
                                }

                                @Override
                                protected void posAction() {
                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            view.hide();
                                        }
                                    });
                                    fireEvent(new IncluirJogadorEvent(m));
                                }
                            })));
        
        registerAction(view.getExcluirButton(), 
                TransactionalAction.build()
                    .persistenceCtxOwner(IncluirJogadorController.this)
                    .addAction(new AbstractAction() {
                        private Jogador m;
                        
                        @Override
                        protected void action() {
                            Integer id = view.getJogadorId();
                            if (id != null) {
                                JogadorDAO dao = new JogadorDAOJPA(getPersistenceContext());
                                m = dao.findById(id);
                                if (m != null) { 
                                    dao.remove(m);
                                }
                            }
                        }
                        
                        @Override
                        public void posAction() {
                            view.hide();
                            fireEvent(new DeletarJogadorEvent(m));
                        }
                    }));
    }
    
    public void show() {
        loadPersistenceContext(((PersistenceController) getParentController()).getPersistenceContext());
        view.show();
    }
    
    public void show(Jogador m) {
        view.setJogador(m);
        view.setTitle("Editar Jogador");
        show();
    }
    
    @Override
    protected void cleanUp() {
        view.setTitle("Incluir Jogador");
        view.resetForm();
        
        super.cleanUp();
    }
}
