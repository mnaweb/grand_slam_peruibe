package br.com.mna.jfx.controller;

import br.com.mna.jfx.action.AbstractAction;
import br.com.mna.jfx.dao.JogadorDAO;
import br.com.mna.jfx.dao.JogadorDAOJPA;
import br.com.mna.jfx.event.AbstractEventListener;
import br.com.mna.jfx.event.AtualizarListarJogadorEvent;
import br.com.mna.jfx.event.BuscarJogadorEvent;
import br.com.mna.jfx.event.DeletarJogadorEvent;
import br.com.mna.jfx.event.IncluirJogadorEvent;
import br.com.mna.jfx.model.Jogador;
import br.com.mna.jfx.ui.ListaJogadoresView;
import br.com.mna.jfx.util.JPAUtil;
import java.util.List;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * Define a <code>Controller</code> principal do sistema, responsável por gerir a tela com a lista de <code>Mercadoria</code>.
 * 
 * @see br.com.yaw.jfx.controller.PersistenceController
 * 
 * @author YaW Tecnologia
 */
public class ListaJogadorController extends PersistenceController {

    private ListaJogadoresView view;
    private IncluirJogadorController incluirController;
    private BuscarJogadorController buscarController;

    public ListaJogadorController() {
        loadPersistenceContext();
        this.view = new ListaJogadoresView();
        this.incluirController = new IncluirJogadorController(this);
        this.buscarController = new BuscarJogadorController(this);
                
        registerAction(view.getNewButton(), new AbstractAction() {
            @Override
            protected void action() {
                ListaJogadorController.this.incluirController.show();
            }
        });
        
        registerAction(view.getFindButton(), new AbstractAction() {
            @Override
            protected void action() {
                ListaJogadorController.this.buscarController.show();
            }
        });
        
        registerAction(view.getRefreshButton(), new AbstractAction() {
            @Override
            protected void action() {
                refreshTable();
            }
        });
        
        view.getTabela().setMouseEvent(new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent t) {
                if (t.getClickCount() == 2) {
                    Jogador m = view.getTabela().getMercadoriaSelected();
                    if (m != null) {
                        ListaJogadorController.this.incluirController.show(m);
                    }
                }
            }
        });
        
        registerEventListener(IncluirJogadorEvent.class, new AbstractEventListener<IncluirJogadorEvent>() {
            @Override
            public void handleEvent(IncluirJogadorEvent event) {
                refreshTable();
            }
        });
        
        registerEventListener(DeletarJogadorEvent.class, new AbstractEventListener<DeletarJogadorEvent>() {
            @Override
            public void handleEvent(DeletarJogadorEvent event) {
                refreshTable();
            }
        });
        
        registerEventListener(AtualizarListarJogadorEvent.class, new AbstractEventListener<AtualizarListarJogadorEvent>() {
            @Override
            public void handleEvent(AtualizarListarJogadorEvent event) {
                refreshTable();
            }
        });
        
        registerEventListener(BuscarJogadorEvent.class, new AbstractEventListener<BuscarJogadorEvent>() {
            @Override
            public void handleEvent(BuscarJogadorEvent event) {
                List<Jogador> list = event.getTarget();
                if (list != null) {
                    refreshTable(event.getTarget());
                }
            }
        });
        
        refreshTable();
    }
    
    @Override
    public void cleanUp() {
        super.cleanUp();
        JPAUtil.closeEntityManagerFactory();
    }
    
    private void refreshTable() {
        refreshTable(null);
    }
    
    private void refreshTable(List<Jogador> list) {
        view.addTransition();
        if (list != null) {
            view.refreshTable(list);
            return;
        }
        
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                JogadorDAO dao = new JogadorDAOJPA(getPersistenceContext());
                view.refreshTable(dao.getAll());
            }
        });
    }
}
