package br.com.mna.jfx.controller;

import br.com.mna.jfx.action.AbstractAction;
import br.com.mna.jfx.ui.ListaJogadoresView;
import br.com.mna.jfx.ui.PrincipalView;
import br.com.mna.jfx.ui.SobreView;
import br.com.mna.jfx.util.JPAUtil;
import javafx.stage.Stage;

/**
 * Define a <code>Controller</code> principal do sistema, responsável por gerir a tela com a lista de <code>Mercadoria</code>.
 * 
 * @see br.com.yaw.jfx.controller.PersistenceController
 * 
 * @author YaW Tecnologia
 */
public class PrincipalController extends PersistenceController {

    private PrincipalView view;

    public PrincipalController(final Stage mainStage) {
        loadPersistenceContext();
        this.view = new PrincipalView(mainStage);
        
        final SobreView sobreView = new SobreView();
        
        final ListaJogadoresView listaJogadoresView = new ListaJogadoresView();
                
        registerAction(view.getMenuSobre(), new AbstractAction() {
            @Override
            public void action() {
                sobreView.show();
            }
        });

        registerAction(view.getMenuJogador(), new AbstractAction() {
            @Override
            public void action() {
                listaJogadoresView.show();
            }
        });
                
    }
    
    @Override
    public void cleanUp() {
        super.cleanUp();
        JPAUtil.closeEntityManagerFactory();
    }
    
}
