package br.com.mna.jfx.app;

import br.com.mna.jfx.controller.PrincipalController;
import java.util.Locale;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Ponto de entrada da aplicacão.
 * 
 * @author YaW Tecnologia
 */
public class GrandSlamPeruibeApp extends Application {
    
    private PrincipalController controller;
    
    @Override
    public void start(Stage stage){
        Locale.setDefault(new Locale("pt","BR"));
        controller = new PrincipalController(stage);
    }

    @Override
    public void stop() throws Exception {
        controller.cleanUp();
    }
    
    public static void main(String[] args) {
        GrandSlamPeruibeApp.launch(args);
    }
    
}
