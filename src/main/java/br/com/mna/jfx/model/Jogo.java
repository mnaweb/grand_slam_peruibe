package br.com.mna.jfx.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "jogo")
public class Jogo implements AbstractEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @NotNull
    @ManyToOne
    private Jogador jogador1;

    @NotNull
    @ManyToOne
    private Jogador jogador2;
    
    @Min(value=0)
    @Max(value=20)
    private Integer jogador1Set1;

    @Min(value=0)
    @Max(value=20)
    private Integer jogador2Set1;

    public Jogo() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Jogador getJogador1() {
        return jogador1;
    }

    public void setJogador1(Jogador jogador1) {
        this.jogador1 = jogador1;
    }

    public Jogador getJogador2() {
        return jogador2;
    }

    public void setJogador2(Jogador jogador2) {
        this.jogador2 = jogador2;
    }

    public Integer getJogador1Set1() {
        return jogador1Set1;
    }

    public void setJogador1Set1(Integer jogador1Set1) {
        this.jogador1Set1 = jogador1Set1;
    }

    public Integer getJogador2Set1() {
        return jogador2Set1;
    }

    public void setJogador2Set1(Integer jogador2Set1) {
        this.jogador2Set1 = jogador2Set1;
    }

    @Override
    public String toString() {
        return "[ " + jogador1.getNome() + " - " + jogador2.getNome() + " ]";
    }
}