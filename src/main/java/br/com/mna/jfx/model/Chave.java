package br.com.mna.jfx.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "chave")
public class Chave implements AbstractEntity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @NotNull
    @ManyToOne
    private Jogo jogo1;

    @NotNull
    @ManyToOne
    private Jogo jogo2;

    @OneToOne
    private Chave chaveJogo1;

    @OneToOne
    private Chave chaveJogo2;

    public Chave() {
    }

    @Override
    public Integer getId() {
        return id;
    }

    public Jogo getJogo1() {
        return jogo1;
    }

    public void setJogo1(Jogo jogo1) {
        this.jogo1 = jogo1;
    }

    public Jogo getJogo2() {
        return jogo2;
    }

    public void setJogo2(Jogo jogo2) {
        this.jogo2 = jogo2;
    }

    public Chave getChaveJogo1() {
        return chaveJogo1;
    }

    public void setChaveJogo1(Chave chaveJogo1) {
        this.chaveJogo1 = chaveJogo1;
    }

    public Chave getChaveJogo2() {
        return chaveJogo2;
    }

    public void setChaveJogo2(Chave chaveJogo2) {
        this.chaveJogo2 = chaveJogo2;
    }

    @Override
    public String toString() {
        return "[ " + jogo1.toString() + " - " + jogo2.toString() + " ]";
    }
}