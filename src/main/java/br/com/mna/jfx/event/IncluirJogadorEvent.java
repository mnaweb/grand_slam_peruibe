package br.com.mna.jfx.event;

import br.com.mna.jfx.model.Jogador;

/**
 * Evento deve ser gerado durante a inclusão de uma <code>Mercadoria</code>.
 * 
 * <p>Recebe a referência da <code>Mercadoria</code> que foi incluida.</p>
 * 
 * @author YaW Tecnologia
 */
public class IncluirJogadorEvent extends AbstractEvent<Jogador> {
	
    public IncluirJogadorEvent(Jogador m) {
        super(m);
    }
    
}
