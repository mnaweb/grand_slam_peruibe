package br.com.mna.jfx.event;

import br.com.mna.jfx.model.Jogador;
import java.util.List;

/**
 * Evento deve ser gerado durante a pesquisa de mercadorias.
 * 
 * <p>
 *  Recebe um <code>List</code> com a(s) <code>Mercadoria<code>(s) encontrada(s).
 * </p>
 * 
 * @author YaW Tecnologia
 */
public class BuscarJogadorEvent extends AbstractEvent<List<Jogador>> {
    
    public BuscarJogadorEvent(List<Jogador> m) {
        super(m);
    }
    
}
