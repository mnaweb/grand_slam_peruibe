package br.com.mna.jfx.event;

/**
 * Evento deve ser gerado quando for necessário atualizar a tabela de mercadorias.
 * 
 * @author YaW Tecnologia
 */
public class AtualizarListarJogadorEvent extends AbstractEvent<Object> {
    
    public AtualizarListarJogadorEvent() {
        super(null);
    }
    
}
