package br.com.mna.jfx.event;

import br.com.mna.jfx.model.Jogador;

/**
 * Evento deve ser gerado durante a exclusão de uma <code>Mercadoria</code>.
 * 
 * <p>Recebe a referência da <code>Mercadoria</code> que foi removida.</p>
 * 
 * @author YaW Tecnologia
 */
public class DeletarJogadorEvent extends AbstractEvent<Jogador> {
    
    public DeletarJogadorEvent(Jogador m) {
        super(m);
    }
    
}
